#include <iostream>


#ifndef LINKLIST_H_
#define LINKLIST_H_


/*
 * TO DO:
 * 	insert() cannot access at head or tail
 */






struct node{
	int data;
	node *next;
};





class list{
	private:
		node *head;
		node *tail;


	public:
		list(){
			head = NULL;
			tail = NULL;
		}




		void display(){						//display the list
			node *temp = new node;
			temp = head;

			while(temp != NULL){
				std::cout << temp -> data << "\t";
				temp = temp -> next;
			}
			std::cout << "\n";
		}



		int at(int loc){							//returns the value at specified location
			node *temp = new node;
			temp = head;

			for(int x = 0; x < loc; ++x){
				temp = temp -> next;
			}

			return temp -> data;
		}



		int size(){						//returns size of list
			int size;
			node *temp = new node;
			temp = head;

			while(temp != NULL){
				temp = temp -> next;
				size += 1;
			}

			return size;
		}




		int sum(){					//returns sum of values in list
			int sum;
			node *temp;
			temp = head;			//I don't know why this would ever be useful bu

			while(temp != NULL){
				sum += temp -> data;
				temp = temp -> next;
			}
			return sum;
		}





		/*
		 *>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		 */



		//node insertion functions
		void push_front(int val){			//insert a new node at the head of the list
			node *temp = new node;
			temp -> data = val;
			temp -> next = head;
			head = temp;
		}



		void push_back(int val){			//add a new node to the back of the list list
			node *temp = new node;
			temp -> data = val;
			temp -> next = NULL;

				if(head == NULL){
					head = temp;
					tail = temp;
					temp = NULL;
				}

				else{
					tail -> next = temp;
					tail = temp;
				}
		}




		void insert(int val, int loc){			//insert a new node at specified loc point in the list
			node *pre = new node;
			node *current = new node;
			node *temp = new node;

			current = head;

			for(int x = 0; x < loc; ++x){
				pre = current;
				current = current -> next;
			}

			temp -> data = val;
			pre -> next = temp;
			temp -> next = current;
		}





		/*
		 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		 */


		//node deletion functions
		void delete_front(){				//delete node from front of list
			node *temp = new node;
			temp = head;
			head = head -> next;
			delete temp;
		}




		void delete_back(){					//deletes node at back of list
			node *current = new node;
			node *pre = new node;
			current = head;

			while(current != NULL){
				pre = current;
				current = current -> next;
			}

			tail = pre;
			pre -> next = NULL;
			delete current;
		}



		void remove(int loc){		//deletes node at location loc from list
			node *current;
			node *pre;
			current = head;

			for(int x = 1; x < loc; ++x){
				pre = current;
				current = current -> next;
			}
			pre -> next = current -> next;
			delete current;
		}

};


#endif /* LINKLIST_H_ */
